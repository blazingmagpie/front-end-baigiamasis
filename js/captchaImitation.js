function Captcha(canvasElement){;
    let c = canvasElement.getContext("2d");
    this.generate = function(){
        let value = getRandomValue();
        this.verify = getVerification(value);

        c.clearRect(0, 0, canvasElement.width, canvasElement.height)
        drawLetters(value);
    }
    this.verify;
    let getVerification = (savedValue) => {
        return (value) => {
            return savedValue === value.toUpperCase();
        }
    }

    let random = (min, max) => Math.floor(Math.random()*(max-min))+min;

    let getRandomValue = () =>{
        const validChars = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", 
                            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        let value = "";
        for(let i = 0; i< 6; i++){
            value += validChars[Math.floor(Math.random()*34)];
        }
        return value;
    }

    let drawLetters = (value) =>{
        let offset = 0, prevY = random(30,canvasElement.height);
        let pickRandom = (...values) => values[random(0, values.length)];
        let generateFont = () => `${random(30, 40)}px ${pickRandom("Arial")}`;
        let generateColor = () => pickRandom("#000", "#444", "#888", "#d00", "#083", "#208", "#60a");
        let lowMargin = (value, atLeast) => value > atLeast ? value : atLeast;
        let highMargin = (value, atMost) => value < atMost ? value : atMost;
        for(let i = 0; i < value.length; i++){
            c.font = generateFont();
            c.fillStyle =generateColor();
            let x = random(20, 30), y = random(lowMargin(prevY-15, 30),highMargin(prevY+15, 100));
            offset += x;
            c.fillText(value[i], offset, y);
        }
    }
}